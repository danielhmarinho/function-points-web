FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /function-points-web
WORKDIR /function-points-web
ADD Gemfile /function-points-web/Gemfile
ADD Gemfile.lock /function-points-web/Gemfile.lock
RUN bundle install
ADD . /function-points-web
