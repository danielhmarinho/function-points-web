class TransactionFunctionsController < ApplicationController
  before_action :set_transaction_function, only: [:show, :edit, :update, :destroy]

  # GET /transaction_functions
  # GET /transaction_functions.json
  def index
    @transaction_functions = TransactionFunction.all
  end

  # GET /transaction_functions/1
  # GET /transaction_functions/1.json
  def show
  end

  # GET /transaction_functions/new
  def new
    @transaction_function = TransactionFunction.new
  end

  # GET /transaction_functions/1/edit
  def edit
  end

  # POST /transaction_functions
  # POST /transaction_functions.json
  def create
    @transaction_function = TransactionFunction.new(transaction_function_params)

    respond_to do |format|
      if @transaction_function.save
        format.html { redirect_to @transaction_function, notice: 'Transaction function was successfully created.' }
        format.json { render :show, status: :created, location: @transaction_function }
      else
        format.html { render :new }
        format.json { render json: @transaction_function.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transaction_functions/1
  # PATCH/PUT /transaction_functions/1.json
  def update
    respond_to do |format|
      if @transaction_function.update(transaction_function_params)
        format.html { redirect_to @transaction_function, notice: 'Transaction function was successfully updated.' }
        format.json { render :show, status: :ok, location: @transaction_function }
      else
        format.html { render :edit }
        format.json { render json: @transaction_function.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transaction_functions/1
  # DELETE /transaction_functions/1.json
  def destroy
    @transaction_function.destroy
    respond_to do |format|
      format.html { redirect_to transaction_functions_url, notice: 'Transaction function was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction_function
      @transaction_function = TransactionFunction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_function_params
      params.require(:transaction_function).permit(:requirement_name, :function, :kind, :qt_alr, :desc_alr, :qt_der, :desc_der, :obs)
    end
end
