class CountsController < ApplicationController
  before_action :set_count, only: [:show, :edit, :update, :destroy]

  # GET /counts
  # GET /counts.json
  def index
    @counts = Count.all
  end

  # GET /counts/1
  # GET /counts/1.json
  def show
    result_data = []
    result_transaction = []
    @project = Project.find(params[:id])
    @count = Count.find(params[:id])
    @data_functions = @count.data_functions
    @transaction_functions = @count.transaction_functions

  #set complexity of data functions
    @data_functions.each do |data|
      if (data.qt_rlr <= 1 && data.qt_der < 50) || (data.qt_rlr.between?(2,5) && data.qt_der < 20)
        complexity = 'baixa'
      elsif (data.qt_rlr <= 1 && data.qt_der > 50) || (data.qt_rlr.between?(2,5) && qt_der.between?(20,50)) || (qt_rlr > 5 && qt_der < 20)
        complexity = 'media'
      else
        complexity = 'alta'
      end

  #set value for complexity
      if complexity == 'baixa' && data.kind == 'ali'
        value = 7
        result_data << value
      elsif complexity == 'media' && data.kind =='ali'
        value = 10
        result_data << value
      elsif complexity == 'alta' && data.kind == 'ali'
        value = 15
        result_data << value
      elsif complexity == 'baixa' && data.kind == 'aie'
        value = 5
        result_data << value
      elsif complexity == 'media' && data.kind = 'aie'
        value = 7
        result_data << value
      else
        value = 10
        result_data << value
      end
    end

  @transaction_functions.each do |transaction|
      if transaction.kind == 'ee'
        if (transaction.qt_alr < 2 && transaction.qt_der < 5) || (transaction.qt_alr < 2 && transaction.qt_der.between?(5,15)) ||
           (transaction.qt_alr == 2 && transaction.qt_der.between?(5,15))
            complexity = 'baixa'
        elsif (transaction.qt_alr < 2 && transaction.qt_der > 15) || (transaction.qt_alr == 2 && transaction.qt_der.between?(5,15)) ||
              (transaction.qt_alr > 2 && transaction.qt_der < 5)
                complexity = 'media'
        else
          complexity = 'alta'
        end
      else
        if (transaction.qt_alr < 2 && transaction.qr_der < 6) || (transaction.qt_alr < 2 && transaction.qr_der.between?(6,19)) ||
          (transaction.qt_alr.between?(2,3) && transaction.qt_alr < 6)
          complexity= 'baixa'
        elsif (transaction.qt_alr < 2 && transaction.qr_der > 19) || (transaction.qt_alr.between?(2,3) && transaction.qr_der.between?(6,19)) ||
          (transaction.qt_alr > 3 && transaction.qt_alr < 6)
          complexity = 'media'
        else
          complexity = 'alta'
        end
      end
      if complexity == 'baixa' && (transaction.kind == 'ee' || transaction.kind == 'ce')
        value = 3
        result_transaction << value
      elsif complexity == 'media' && (transaction.kind == 'ee' || transaction.kind == 'ce')
        value = 4
        result_transaction << value
      elsif complexity == 'alta' && (transaction.kind == 'ee' || transaction.kind == 'ce')
        value = 6
        result_transaction << value
      elsif complexity == 'baixa' && transaction.kind == 'se'
        value = 4
        result_transaction << value
      elsif complexity == 'media' && transaction.kind == 'se'
        value = 5
        result_transaction << value
      else
        value = 7
        result_transaction << value
      end

    end
    result_total = []
    result_total = result_transaction + result_data
    result_final = result_total.inject(:+)
    cost = result_final*@count.point_cost
    @project.update_attributes(cost: cost)
    @project.save
    @count.update_attributes(result: result_final)
    @count.save
  end

  # GET /counts/new
  def new
    @count = Count.new
  end

  # GET /counts/1/edit
  def edit
  end

  # POST /counts
  # POST /counts.json
  def create
    @count = Count.new(count_params)

    respond_to do |format|
      if @count.save
        format.html { redirect_to @count, notice: 'Count was successfully created.' }
        format.json { render :show, status: :created, location: @count }
      else
        format.html { render :new }
        format.json { render json: @count.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /counts/1
  # PATCH/PUT /counts/1.json
  def update
    respond_to do |format|
      if @count.update(count_params)
        format.html { redirect_to @count, notice: 'Count was successfully updated.' }
        format.json { render :show, status: :ok, location: @count }
      else
        format.html { render :edit }
        format.json { render json: @count.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /counts/1
  # DELETE /counts/1.json
  def destroy
    @count.destroy
    respond_to do |format|
      format.html { redirect_to counts_url, notice: 'Count was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_count
      @count = Count.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def count_params
      params.require(:count).permit(:count_date, :result, :style, :pattern, :point_cost, :baseline)
    end

    def transaction_function_params
      params.require(:transaction_function).permit(:requirement_name, :function, :kind, :qt_alr, :desc_alr, :qt_der, :desc_der, :obs)
    end

    def data_function_params
      params.require(:data_function).permit(:requirement_name, :function, :kind, :qt_rlr, :desc_rlr, :qt_der, :desc_der, :obs)
    end
end
