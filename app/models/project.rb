class Project < ApplicationRecord
  has_many :counts
  mount_uploader :image, ImageUploader
end
