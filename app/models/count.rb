class Count < ApplicationRecord
  belongs_to :project
  has_many :data_functions
  has_many :transaction_functions

end
