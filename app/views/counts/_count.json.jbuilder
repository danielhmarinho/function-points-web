json.extract! count, :id, :count_date, :result, :created_at, :updated_at
json.url count_url(count, format: :json)
