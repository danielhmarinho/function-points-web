json.extract! project, :id, :name, :start_date, :finish_date, :cost, :created_at, :updated_at
json.url project_url(project, format: :json)
