json.extract! data_function, :id, :requirement_name, :function, :kind, :qt_rlr, :desc_rlr, :qt_der, :desc_der, :obs, :created_at, :updated_at
json.url data_function_url(data_function, format: :json)
