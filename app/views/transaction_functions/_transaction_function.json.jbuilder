json.extract! transaction_function, :id, :requirement_name, :function, :kind, :qt_alr, :desc_alr, :qt_der, :desc_der, :obs, :created_at, :updated_at
json.url transaction_function_url(transaction_function, format: :json)
