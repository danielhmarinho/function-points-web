module ProjectsHelper

  def graph_generator
    @project = Project.find(params[:id])
    @counts = @project.counts

    data = @counts.select(:result).pluck(:id, :result)

    line_chart [{name: "Evolução das contagens", data: data}],
    width: "100%",
    height: "250px",
    legend: 'bottom'

  end
end
