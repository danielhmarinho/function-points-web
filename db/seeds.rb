
Count.create(count_date: "2017-06-25", result: nil, style: 'Desenvolvimento', pattern: 'Aplicação', project_id: 1, point_cost: 2, baseline: true)

data_functions = DataFunction.create([{requirement_name: nil, function: nil, kind: 'ali', qt_rlr: 2, desc_rlr: nil, qt_der: 10, desc_der: nil, obs: nil, count_id: 1},
{requirement_name: nil, function: nil, kind: 'ali', qt_rlr: 1, desc_rlr: nil, qt_der: 7, desc_der: nil, obs: nil, count_id: 1},
{requirement_name: nil, function: nil, kind: 'ali', qt_rlr: 3, desc_rlr: nil, qt_der: 15, desc_der: nil, obs: nil, count_id: 1}])

transaction_functions = TransactionFunction.create([{requirement_name: nil, function: nil, kind: 'ee', qt_alr: 2, desc_alr: nil, qt_der: 5, desc_der: nil, obs: nil, count_id: 1},
  {requirement_name: nil, function: nil, kind: 'ee', qt_alr: 3, desc_alr: nil, qt_der: 7, desc_der: nil, obs: nil, count_id: 1},
  requirement_name: nil, function: nil, kind: 'ee', qt_alr: 1, desc_alr: nil, qt_der: 3, desc_der: nil, obs: nil, count_id: 1])
