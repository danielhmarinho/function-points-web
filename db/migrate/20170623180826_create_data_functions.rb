class CreateDataFunctions < ActiveRecord::Migration[5.0]
  def change
    create_table :data_functions do |t|
      t.string :requirement_name
      t.string :function
      t.string :kind
      t.integer :qt_rlr
      t.text :desc_rlr
      t.integer :qt_der
      t.text :desc_der
      t.text :obs
      t.belongs_to :count, index: true

      t.timestamps
    end
  end
end
