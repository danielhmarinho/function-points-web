class CreateCounts < ActiveRecord::Migration[5.0]
  def change
    create_table :counts do |t|
      t.date :count_date
      t.integer :result
      t.string :style
      t.string :pattern
      t.float :point_cost
      t.boolean :baseline
      t.belongs_to :project, index: true

      t.timestamps
    end
  end
end
