class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.date :start_date
      t.date :finish_date
      t.float :cost
      t.string :developer
      t.string :url
      t.string :image

      t.timestamps
    end
  end
end
