require "rails_helper"

RSpec.describe DataFunctionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/data_functions").to route_to("data_functions#index")
    end

    it "routes to #new" do
      expect(:get => "/data_functions/new").to route_to("data_functions#new")
    end

    it "routes to #show" do
      expect(:get => "/data_functions/1").to route_to("data_functions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/data_functions/1/edit").to route_to("data_functions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/data_functions").to route_to("data_functions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/data_functions/1").to route_to("data_functions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/data_functions/1").to route_to("data_functions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/data_functions/1").to route_to("data_functions#destroy", :id => "1")
    end

  end
end
