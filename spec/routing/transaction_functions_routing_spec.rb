require "rails_helper"

RSpec.describe TransactionFunctionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/transaction_functions").to route_to("transaction_functions#index")
    end

    it "routes to #new" do
      expect(:get => "/transaction_functions/new").to route_to("transaction_functions#new")
    end

    it "routes to #show" do
      expect(:get => "/transaction_functions/1").to route_to("transaction_functions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/transaction_functions/1/edit").to route_to("transaction_functions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/transaction_functions").to route_to("transaction_functions#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/transaction_functions/1").to route_to("transaction_functions#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/transaction_functions/1").to route_to("transaction_functions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/transaction_functions/1").to route_to("transaction_functions#destroy", :id => "1")
    end

  end
end
