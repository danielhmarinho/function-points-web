Rails.application.routes.draw do
  resources :transaction_functions
  resources :data_functions
  resources :counts
  resources :projects
  resources :organizations
  devise_for :users
  resources :users

  devise_scope :user do
    authenticated :user do
      root 'projects#index'
end

  unauthenticated do
    root 'devise/sessions#new', as: :unauthenticated_root
  end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
